from django.shortcuts import render,redirect
from keyring import set_password
from . models import User
from . forms import CustomRegisterForm
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages
# Create your views here.

def loginuser(request):
    if request.method=='POST':
        form=AuthenticationForm(request,data=request.POST)
        if form.is_valid():
            username=form.cleaned_data['username']
            password=form.cleaned_data['password']
            user=authenticate(username=username,password=password)
            if user is not None:
                print('user')
                login(request,user)
                return redirect('home')
            
            messages.error(request,'User does not exist')
    else:
        form=AuthenticationForm()
    return render(request,'login/loginuser.html',{'form':form})

def register(request):
    if request.method=='POST':
        form=CustomRegisterForm(request.POST)
        if form.is_valid():
            username=form.cleaned_data['username']
            email=form.cleaned_data['email']
            password=form.cleaned_data['password']
            cpassword=form.cleaned_data['confirm_password']
            print(username,email)
            if password != cpassword:
                messages.error(request,'passwords do not match')
                return redirect('register')
            user=User.objects.filter(username=username)
            if user.count():
                messages.error(request,'username already exists')
                return redirect('register')
            mail=User.objects.filter(email=email)
            if mail.count():
                messages.error(request,'email already exists')
                return redirect('register')
            obj=User.objects.create_user(username=username,email=email,password=password)
            messages.success(request,'User registered Successfully')
            return redirect('loginuser')    
    else:
        form=CustomRegisterForm()
    return render(request,'login/register.html',{'form':form})

def home(request):    
    if not request.user.is_authenticated:
        return redirect('loginuser')    
    return render(request,'login/home.html')

def logoutuser(request):
    if not request.user.is_authenticated:
        return redirect('loginuser') 
    logout(request)
    return redirect('loginuser')