from django import forms
from django.forms import ModelForm
from . models import User

class CustomRegisterForm(ModelForm):
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_password=forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model=User
        fields=('username','email','password')

    


# class CustomLoginForm(ModelForm):
#     password=forms.CharField(widget=forms.PasswordInput())
#     class Meta:
#         model=User
#         fields=['username','password']